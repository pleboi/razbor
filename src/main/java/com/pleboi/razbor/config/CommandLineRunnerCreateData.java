package com.pleboi.razbor.config;

import com.pleboi.razbor.model.dto.ParticipantDto;
import com.pleboi.razbor.model.entity.LiveStream;
import com.pleboi.razbor.model.entity.Participant;
import com.pleboi.razbor.repository.LiveStreamRepository;
import com.pleboi.razbor.repository.ParticipantRepository;
import com.pleboi.razbor.utils.SpreadsheetHelper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Profile("dev-data")
@Component
@Log4j2
public class CommandLineRunnerCreateData implements CommandLineRunner {

    @Value("${razbor.value.pathFileWithData}")
    private String pathFileWithData;

    @Autowired
    private SpreadsheetHelper spreadsheetHelper;

    @Autowired
    private ParticipantRepository participantRepo;

    @Autowired
    private LiveStreamRepository liveStreamRepo;

    @Override
    public void run(String... args) throws Exception {
        try {
            File file = new File(pathFileWithData);
            List<Participant> participants = spreadsheetHelper.excelToParticipant(new FileInputStream(file)).stream()
                    .map(ParticipantDto::toParticipant)
                    .toList();
            participantRepo.saveAll(participants);

            AtomicInteger count = new AtomicInteger(0);
            AtomicInteger streamNumber = new AtomicInteger(1);
            List<LiveStream> liveStreams = participants.stream().limit(5).map(p -> {
                count.incrementAndGet();

                LiveStream ls = new LiveStream();
                ls.setId(p.getId());
                ls.setSeasonNumber(1);
                ls.setStreamNumber(streamNumber.get());

                if (count.get() == 5) {
                    streamNumber.incrementAndGet();
                }

                return ls;
            }).toList();

            liveStreamRepo.saveAll(liveStreams);

            log.info("Test data is initialized");
        } catch (IOException e) {
            log.info("Test data is not initialized. Reason: {}. Path: {}", e.getMessage(), pathFileWithData);
        }

    }
}
