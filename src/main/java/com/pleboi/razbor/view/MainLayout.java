package com.pleboi.razbor.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.theme.lumo.LumoUtility;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.security.PermitAll;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@SpringComponent
@Scope(value = "vaadin-ui", proxyMode = ScopedProxyMode.INTERFACES)
@PermitAll
@RequiredArgsConstructor
public class MainLayout extends AppLayout {

    @Autowired
    private final HorizontalLayout logoutLayout;

    @PostConstruct
    private void postConstruct() {
        H1 title = new H1("Разбор");
        title.getStyle().set("font-size", "var(--lumo-font-size-l)")
                .set("left", "var(--lumo-space-l)").set("margin", "0")
                .set("position", "absolute");

        HorizontalLayout header = new HorizontalLayout(new DrawerToggle(), title, logoutLayout);

        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        header.expand(title);

        header.setWidthFull();
        header.addClassNames(
                LumoUtility.Padding.Vertical.NONE,
                LumoUtility.Padding.Horizontal.MEDIUM);

        addToNavbar(title, getTabs(), logoutLayout);
    }

    private Tabs getTabs() {
        Tabs tabs = new Tabs();
        tabs.getStyle().set("margin", "auto");
        tabs.add(
                createTab("Заявки", ParticipantView.class),
                createTab("Стримы", LiveStreamView.class)
        );
        return tabs;
    }

    private Tab createTab(String viewName, Class<? extends Component> viewClass) {
        return new Tab(new RouterLink(viewName, viewClass));
    }
}
