package com.pleboi.razbor.view.config;

import com.pleboi.razbor.service.SecurityService;
import com.pleboi.razbor.view.components.UploadDiv;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ComponentsConfig {

    @Bean
    @UIScope
    public Dialog uploadDialog(UploadDiv uploadDiv) {
        Dialog dialog = new Dialog();
        dialog.add(new VerticalLayout(uploadDiv));

        return dialog;
    }

    @Bean
    @UIScope
    @Profile("security")
    public HorizontalLayout logoutLayoutSecurityOn(SecurityService securityService) {
        Button button = new Button("Выйти", e -> securityService.logout());
        button.getStyle().set("right", "var(--lumo-space-l)").set("margin", "0");

        return new HorizontalLayout(button);
    }

    @Bean
    @UIScope
    @Profile("!security")
    public HorizontalLayout logoutLayoutSecurityOff() {
        return new HorizontalLayout();
    }

    @Bean
    @UIScope
    @Profile("security")
    public LoginForm loginForm() {
        LoginI18n i18n = LoginI18n.createDefault();

        LoginI18n.Form i18nForm = i18n.getForm();
        i18nForm.setTitle("Авторизоваться");
        i18nForm.setUsername("Имя пользователя");
        i18nForm.setPassword("Пароль");
        i18nForm.setSubmit("Войти");
        i18nForm.setForgotPassword("");
        i18n.setForm(i18nForm);

        LoginI18n.ErrorMessage i18nErrorMessage = i18n.getErrorMessage();
        i18nErrorMessage.setTitle("Неверное имя пользователя или пароль");
        i18nErrorMessage.setMessage("Проверьте правильность имени пользователя и пароля и повторите попытку.");
        i18n.setErrorMessage(i18nErrorMessage);

        LoginForm loginForm = new LoginForm();
        loginForm.setI18n(i18n);
        loginForm.getElement().setAttribute("no-autofocus", "");

        return loginForm;
    }
}
