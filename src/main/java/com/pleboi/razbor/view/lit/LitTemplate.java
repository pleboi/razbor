package com.pleboi.razbor.view.lit;

public class LitTemplate {
    public final static String LIT_LINK =
            """
            <vaadin-vertical-layout>
                <a target="_blank" rel="noopener noreferrer" href="${item.link}" class="vaadin-button">${item.text}</a>
            </vaadin-vertical-layout>
            """;
}
