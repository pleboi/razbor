package com.pleboi.razbor.view;

import com.pleboi.razbor.model.entity.Season;
import com.pleboi.razbor.service.SeasonService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.security.PermitAll;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;

import static com.vaadin.flow.component.notification.Notification.Position.BOTTOM_STRETCH;

@RequiredArgsConstructor
@Route(value = "livestreams", layout = MainLayout.class)
@PageTitle("Разбор | Стримы")
@Log4j2
@PermitAll
public class LiveStreamView extends VerticalLayout {

    @Autowired
    private final SeasonService seasonService;

    @PostConstruct
    private void postConstruct() {
        add(newSeasonButton());
    }

    private Button newSeasonButton() {
        Button button = new Button("Добавить сезон");
        button.addClickListener(clickEvent -> {
            Season season = seasonService.addNew();
            Notification.show(String.format("Новый сезон добавлен, #%s", season.getId()), 1000, BOTTOM_STRETCH);
        });

        return button;
    }
}
