package com.pleboi.razbor.view.components;

import com.vaadin.flow.component.upload.UploadI18N;
import com.vaadin.flow.spring.annotation.SpringComponent;

import java.util.Arrays;

@SpringComponent
public class CustomUploadI18N extends UploadI18N {
    public CustomUploadI18N() {
        setError(new Error()
                .setTooManyFiles("Много файлов")
                .setFileIsTooBig("Размер файла слишком большой")
                .setIncorrectFileType("Неправильный тип файла"));

        setUploading(new Uploading()
                .setStatus(new Uploading.Status()
                        .setConnecting("Соединение...")
                        .setProcessing("Файл обрабатывается...")
                        .setHeld("В очереди"))
                .setRemainingTime(new Uploading.RemainingTime()
                        .setPrefix("оставшееся время: ")
                        .setUnknown("оставшееся время: неизвестно"))
                .setError(new Uploading.Error()
                        .setServerUnavailable("Не удалось загрузить, повторите попытку позже")
                        .setUnexpectedServerError("Загрузка не удалась из-за ошибки сервера")
                        .setForbidden("Загрузка недоступна")));

        setUnits(new Units().setSize(Arrays.asList("B", "kB", "MB")));

        setAddFiles(new AddFiles().setOne("Загрузить таблицу..."));

        setDropFiles(new DropFiles().setOne("Перетащите таблицу сюда"));
    }
}
