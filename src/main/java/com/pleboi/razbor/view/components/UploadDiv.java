package com.pleboi.razbor.view.components;

import com.pleboi.razbor.service.impl.ParticipantServiceImpl;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InputStream;

@SpringComponent
@UIScope
@RequiredArgsConstructor
public class UploadDiv extends Div {

    // Microsoft Excel (.xls)
    private final static String FT_MS_AX = "application/vnd.ms-excel";
    private final static String FT_MS_XLS = ".xls";
    // Microsoft Excel (OpenXML, .xlsx)
    private final static String FT_MS_AOX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    private final static String FT_MS_XLSX = ".xlsx";
    // Comma-separated values (.csv)
    private final static String FT_T_XSV = "text/csv";
    private final static String FT_XSV = ".csv";

    private final static int MAX_FILE_SIZE_IN_BYTES = 10 * 1024 * 1024; // 10MB

    @Autowired
    private final ParticipantServiceImpl participantService;

    @Autowired
    private final CustomUploadI18N i18n;

    @PostConstruct
    private void postConstruct() {
        add(createTitle(), createHint(), createUpload());
    }

    private Component createTitle() {
        H4 title = new H4("Загрузить таблицу");
        title.getStyle().set("margin-top", "0");

        return title;
    }

    private Component createHint() {
        Paragraph hint = new Paragraph("Размер файла должен быть меньше или равен 10 МБ.\n" +
                        "Принимаются только один файл Excel формата .xls");
        hint.getStyle()
                .set("color", "var(--lumo-secondary-text-color)")
                .set("white-space", "pre-line");

        return hint;
    }

    private Upload createUpload() {
        MemoryBuffer buffer = new MemoryBuffer();

        Upload upload = new Upload(buffer);

        upload.setAcceptedFileTypes(FT_MS_AX, FT_MS_XLS);
        upload.setMaxFileSize(MAX_FILE_SIZE_IN_BYTES);
        upload.setI18n(i18n);
        decorateI18N();

        upload.addFileRejectedListener(event -> {
            String errorMessage = event.getErrorMessage();

            Notification notification = Notification.show(errorMessage, 5000,
                    Notification.Position.MIDDLE);
            notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
        });

        upload.addSucceededListener(event -> {
            InputStream fileData = buffer.getInputStream();

            String fileName = event.getFileName();
            long contentLength = event.getContentLength();
            String mimeType = event.getMIMEType();

            participantService.saveAll(fileData);
        });

        return upload;
    }

    private void decorateI18N() {
        i18n.getError()
                .setIncorrectFileType("Допустимы файлы в формате .xls");
    }
}
