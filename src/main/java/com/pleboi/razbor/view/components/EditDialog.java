package com.pleboi.razbor.view.components;

import com.pleboi.razbor.model.entity.LiveStream;
import com.pleboi.razbor.model.entity.Participant;
import com.pleboi.razbor.service.LiveStreamService;
import com.pleboi.razbor.service.ParticipantService;
import com.pleboi.razbor.service.SeasonService;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.spring.annotation.UIScope;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@UIScope
@RequiredArgsConstructor
public class EditDialog {

    public interface ChangeHandler {
        void onChange();
    }

    public static final int CHAR_LIMIT = 500;

    private Dialog dialog;
    private ComboBox<Integer> comboBoxSeasons;
    private ComboBox<Integer> comboBoxStream;
    private Anchor vkLink;
    private Anchor storageLink;
    private TextArea textAreaComment;

    private ChangeHandler changeHandler;
    private Participant participant;

    @Autowired
    private final ParticipantService participantService;

    @Autowired
    private final LiveStreamService liveStreamService;

    @Autowired
    private final SeasonService seasonService;

    @PostConstruct
    private void createDialog() {
        dialog = new Dialog();
        dialog.setWidth(500, Unit.PIXELS);

        VerticalLayout mainVertLayout = new VerticalLayout();
        mainVertLayout.setAlignItems(FlexComponent.Alignment.CENTER);

        comboBoxSeasons = new ComboBox<>("Сезон");
        comboBoxSeasons.setHelperText("Укажите сезон");
        comboBoxSeasons.setWidth(225, Unit.PIXELS);

        comboBoxStream = new ComboBox<>("Стрим");
        comboBoxStream.setHelperText("Укажите стрим");
        comboBoxStream.setWidth(225, Unit.PIXELS);

        vkLink = new Anchor();
        vkLink.setTarget("_blank");
        vkLink.setWidth(225, Unit.PIXELS);

        storageLink = new Anchor();
        storageLink.setTarget("_blank");
        storageLink.setWidth(225, Unit.PIXELS);

        textAreaComment = new TextArea();
        textAreaComment.setWidth(450, Unit.PIXELS);
        textAreaComment.setLabel("Комментарий");
        textAreaComment.setMaxLength(CHAR_LIMIT);
        textAreaComment.setValueChangeMode(ValueChangeMode.EAGER);
        textAreaComment.addValueChangeListener(listener -> listener.getSource()
                .setHelperText(getHelperText(listener))
        );

        Button btnSave = new Button("Сохранить", clickEvent -> {
            saveAndReload(participant);
            dialog.close();
        });
        btnSave.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SUCCESS);

        Button btnCancel = new Button("Отмена", clickEvent -> dialog.close());
        btnCancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY, ButtonVariant.LUMO_ERROR);

        HorizontalLayout linkLayout = new HorizontalLayout(vkLink, storageLink);
        linkLayout.setWidth(450, Unit.PIXELS);

        HorizontalLayout comBoxLayout = new HorizontalLayout(comboBoxSeasons, comboBoxStream);
        comBoxLayout.setWidth(450, Unit.PIXELS);

        HorizontalLayout buttonLayout = new HorizontalLayout(btnSave, btnCancel);

        mainVertLayout.add(
                linkLayout,
                comBoxLayout,
                textAreaComment,
                buttonLayout
        );

        dialog.add(mainVertLayout);
    }

    private String getHelperText(AbstractField.ComponentValueChangeEvent<TextArea, String> listener) {
        return Optional.of(listener.getValue().length()).orElse(0) + "/" + CHAR_LIMIT;
    }

    public void open(Participant participant, ChangeHandler handler) {
        this.participant = participant;
        this.changeHandler = handler;
        setFillData();

        dialog.open();
    }

    private void setFillData() {
        vkLink.setHref("https://vk.com/id" + participant.getUserVkId());
        vkLink.setText(participant.getFullName());

        storageLink.setHref(participant.getStorageLink());
        storageLink.setText("Ссылка на диск");

        comboBoxSeasons.setItems(seasonService.getSeasonNumbers());
        comboBoxStream.setItems(liveStreamService.getListNumbers());

        LiveStream liveStream = participant.getLiveStream();
        comboBoxSeasons.setValue(liveStream != null ? liveStream.getSeasonNumber() : null);
        comboBoxStream.setValue(liveStream != null ? liveStream.getStreamNumber() : null);

        textAreaComment.setValue(Optional.ofNullable(participant.getComment()).orElse(""));
    }

    private void saveAndReload(Participant participant) {
        LiveStream liveStream = new LiveStream();
        liveStream.setId(participant.getId());
        liveStream.setSeasonNumber(comboBoxSeasons.getValue());
        liveStream.setStreamNumber(comboBoxStream.getValue());

        participant.setLiveStream(liveStream);
        participant.setComment(textAreaComment.getValue());

        participantService.save(participant);
        changeHandler.onChange();
    }
}
