package com.pleboi.razbor.view;

import com.pleboi.razbor.model.entity.Participant;
import com.pleboi.razbor.service.LiveStreamService;
import com.pleboi.razbor.service.ParticipantService;
import com.pleboi.razbor.service.SeasonService;
import com.pleboi.razbor.view.components.EditDialog;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.security.PermitAll;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.pleboi.razbor.view.lit.LitTemplate.LIT_LINK;

@Route(value = "", layout = MainLayout.class)
@PageTitle("Разбор | Заявки")
@Log4j2
@PermitAll
@RequiredArgsConstructor
public class ParticipantView extends VerticalLayout {
    @Autowired
    private final ParticipantService participantService;

    @Autowired
    private final EditDialog participantEditDialog;

    @Autowired
    private final LiveStreamService liveStreamService;

    @Autowired
    private final SeasonService seasonService;

    @Autowired
    private final Dialog uploadDialog;

    private TextField tfNameColumnFilter;
    private ComboBox<Integer> cbSeasonColumnFilter;
    private ComboBox<Integer> cbStreamColumnFilter;
    private Grid<Participant> grid;

    @PostConstruct
    private void postConstruct() {
        this.grid = createGrid();
        Button uploadButton = createUploadButton(uploadDialog);
        add(prepareFilterFields(), grid, uploadButton);

        setAlignSelf(Alignment.END, uploadButton);
    }

    private Grid<Participant> createGrid() {
        Grid<Participant> grid = new Grid<>(Participant.class, false);

        grid.addComponentColumn(this::createEditBtn)
                .setAutoWidth(true)
                .setFlexGrow(0);

        grid.addColumn(Participant::getFullName)
                .setSortable(true)
                .setAutoWidth(true)
                .setFlexGrow(0)
                .setHeader("Имя");

        grid.addColumn(LitRenderer.<Participant>of(LIT_LINK)
                        .withProperty("link", participant -> "https://vk.com/id" + participant.getUserVkId())
                        .withProperty("text", participant -> "VK"))
                .setAutoWidth(true)
                .setFlexGrow(0)
                .setHeader("Ссылка ВК");

        grid.addColumn(LitRenderer.<Participant>of(LIT_LINK)
                        .withProperty("link", Participant::getStorageLink)
                        .withProperty("text", participant -> "Диск"))
                .setAutoWidth(true)
                .setFlexGrow(0)
                .setHeader("Ссылка на диск");

        grid.addColumn(p -> p.getLiveStream() != null ? p.getLiveStream().getSeasonNumber() : "")
                .setAutoWidth(true)
                .setFlexGrow(0)
                .setSortable(true)
                .setHeader("Сезон");

        grid.addColumn(p -> p.getLiveStream() != null ? p.getLiveStream().getStreamNumber() : "")
                .setAutoWidth(true)
                .setFlexGrow(0)
                .setSortable(true)
                .setHeader("Стрим");

        grid.addColumn(Participant::getComment)
                .setHeader("Комментарий");

        grid.setMultiSort(true, Grid.MultiSortPriority.APPEND);
        grid.setItems(participantService.findAll());

        return grid;
    }

    private Button createEditBtn(Participant participant) {
        return new Button(new Icon(VaadinIcon.PENCIL),
                clickEvent -> participantEditDialog.open(participant, this::updateGrid));
    }

    void updateGrid() {
        grid.setItems(participantService.findAll());
        onFilterChange();
    }

    private HorizontalLayout prepareFilterFields() {
        HorizontalLayout hl = new HorizontalLayout();

        tfNameColumnFilter = nameColumnFilter();
        cbSeasonColumnFilter = createComboBoxColumnFilter("Фильтр по сезонам", seasonService.getSeasonNumbers());
        cbStreamColumnFilter = createComboBoxColumnFilter("Фильтр по стримам", liveStreamService.getListNumbers());

        Button btnResetFilter = new Button(new Icon(VaadinIcon.CLOSE_SMALL));
        btnResetFilter.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_PRIMARY);
        btnResetFilter.setAriaLabel("Reset");
        btnResetFilter.setTooltipText("Сбросить фильтр");
        btnResetFilter.addClickListener(clickEvent -> clearFilter());

        hl.add(tfNameColumnFilter, cbSeasonColumnFilter, cbStreamColumnFilter, btnResetFilter);
        hl.setVerticalComponentAlignment(FlexComponent.Alignment.END, btnResetFilter);

        return hl;
    }

    private TextField nameColumnFilter() {
        TextField tf = new TextField();
        tf.setLabel("Фильтр по имени");
        tf.setValueChangeMode(ValueChangeMode.TIMEOUT);
        tf.addValueChangeListener(event -> this.onFilterChange());
        tf.setWidthFull();

        return tf;
    }

    private ComboBox<Integer> createComboBoxColumnFilter(String label, List<Integer> values) {
        ComboBox<Integer> cb = new ComboBox<>();
        cb.setLabel(label);
        cb.setAllowedCharPattern("[1-9]");
        cb.setItems(values);
        cb.addValueChangeListener(event -> this.onFilterChange());
        cb.setWidthFull();

        return cb;
    }

    public void clearFilter() {
        tfNameColumnFilter.clear();
        cbSeasonColumnFilter.clear();
        cbStreamColumnFilter.clear();
    }

    private void onFilterChange() {
        GridListDataView<Participant> listDataView = grid.getListDataView();
        listDataView.setFilter(item -> {
            boolean nameColumnFilterMatch = true;
            boolean seasonsColumnFilterMatch = true;
            boolean streamColumnFilterMatch = true;

            if (!tfNameColumnFilter.isEmpty()) {
                nameColumnFilterMatch = item.getFullName().toUpperCase()
                        .contains(tfNameColumnFilter.getValue().toUpperCase());
            }

            if (!cbSeasonColumnFilter.isEmpty()) {
                seasonsColumnFilterMatch = item.getLiveStream() != null
                        && item.getLiveStream().getSeasonNumber() == cbSeasonColumnFilter.getValue();
            }

            if (!cbStreamColumnFilter.isEmpty()) {
                streamColumnFilterMatch = item.getLiveStream() != null
                        && item.getLiveStream().getStreamNumber() == cbStreamColumnFilter.getValue();
            }

            return nameColumnFilterMatch && seasonsColumnFilterMatch && streamColumnFilterMatch;
        });
    }

    public Button createUploadButton(Dialog uploadDialog) {
        return new Button("Загрузить таблицу",
                clickEvent -> {
                    uploadDialog.open();
                    uploadDialog.addDialogCloseActionListener(e -> {
                        grid.setItems(participantService.findAll());
                        clearFilter();
                        uploadDialog.close();
                    });
                });
    }
}

