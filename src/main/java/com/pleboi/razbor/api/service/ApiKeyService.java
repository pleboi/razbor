package com.pleboi.razbor.api.service;

public interface ApiKeyService {
    boolean validateApiKey(String apiKey);
}
