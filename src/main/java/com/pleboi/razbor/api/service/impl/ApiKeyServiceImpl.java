package com.pleboi.razbor.api.service.impl;

import com.pleboi.razbor.api.service.ApiKeyService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ApiKeyServiceImpl implements ApiKeyService {

    @Value("${razbor.value.apikey}")
    private String apiKey;

    @Override
    public boolean validateApiKey(String apiKey) {
        return Objects.equals(this.apiKey, apiKey);
    }
}
