package com.pleboi.razbor.api.controller;

import com.pleboi.razbor.api.service.ApiKeyService;
import com.pleboi.razbor.model.dto.ParticipantDto;
import com.pleboi.razbor.model.entity.Participant;
import com.pleboi.razbor.service.ParticipantService;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/public/v1/participant")
@Log4j2
public record ParticipantRestController(
        @Autowired ParticipantService participantService,
        @Autowired ApiKeyService apiKeyService
) {

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Participant> saveParticipant(
            @RequestParam String apiKey,
            @Valid @RequestBody ParticipantDto participantDto
    ) {
        log.info("apiKey: [{}], Participant: [{}]", apiKey, participantDto);

        if (apiKeyService.validateApiKey(apiKey)) {
            return new ResponseEntity<>(participantService.save(participantDto.toParticipant()), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }
}
