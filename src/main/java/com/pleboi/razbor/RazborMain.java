package com.pleboi.razbor;

import com.pleboi.razbor.model.entity.Season;
import com.pleboi.razbor.repository.SeasonRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RazborMain {

    public static void main(String[] args) {
        SpringApplication.run(RazborMain.class, args);
    }

    @Bean
    public CommandLineRunner initInstallData(SeasonRepository repo) {
        return (args) -> {
            repo.save(new Season());
        };
    }
}
