package com.pleboi.razbor.service.impl;

import com.pleboi.razbor.service.SecurityService;
import com.vaadin.flow.spring.security.AuthenticationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("security")
@Service
public record SecurityServiceImpl(
        @Autowired AuthenticationContext authenticationContext
) implements SecurityService {

    @Override
    public void logout() {
        authenticationContext.logout();
    }
}
