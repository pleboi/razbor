package com.pleboi.razbor.service.impl;

import com.pleboi.razbor.service.LiveStreamService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.IntStream;

@Service
@Log4j2
@RequiredArgsConstructor
public class LiveStreamServiceImpl implements LiveStreamService {
    @Value("${razbor.streamCountInSeason}")
    private Integer streamCount;

    @Override
    public List<Integer> getListNumbers() {
        List<Integer> list = IntStream.range(1, streamCount + 1).boxed().toList();
        log.info("streams list {}", list);

        return list;
    }
}
