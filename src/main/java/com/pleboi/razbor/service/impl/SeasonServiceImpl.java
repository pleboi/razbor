package com.pleboi.razbor.service.impl;

import com.pleboi.razbor.model.entity.Season;
import com.pleboi.razbor.repository.SeasonRepository;
import com.pleboi.razbor.service.SeasonService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
public record SeasonServiceImpl(
        @Autowired SeasonRepository repo
) implements SeasonService {

    @Override
    public Season addNew() {
        log.info("trying add new season");
        Season season = repo.save(new Season());

        log.info("new season added [{}]", season);
        return season;
    }

    @Override
    public List<Integer> getSeasonNumbers() {
        List<Integer> list = repo.findAll().stream().map(Season::getId).toList();
        log.info("seasons list {}", list);

        return list;
    }
}
