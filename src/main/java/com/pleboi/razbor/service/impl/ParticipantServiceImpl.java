package com.pleboi.razbor.service.impl;

import com.pleboi.razbor.model.dto.ParticipantDto;
import com.pleboi.razbor.model.entity.Participant;
import com.pleboi.razbor.repository.ParticipantRepository;
import com.pleboi.razbor.service.ParticipantService;
import com.pleboi.razbor.utils.SpreadsheetHelper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;

@Service
@Log4j2
public record ParticipantServiceImpl(
        @Autowired ParticipantRepository repo,
        @Autowired SpreadsheetHelper spreadsheetHelper
) implements ParticipantService {

    @Override
    public List<Participant> saveAll(InputStream inputStream) {
        List<Participant> participants = spreadsheetHelper.excelToParticipant(inputStream).stream()
                .map(ParticipantDto::toParticipant)
                .toList();
        log.info("trying save {} participant(s)", participants.size());

        List<Participant> res = repo.saveAll(participants);
        log.info("new {} participant(s) was saved", res.size());

        return res;
    }

    @Override
    public List<Participant> findAll() {
        List<Participant> all = repo.findAll();
        log.info("was found {} participant(s)", all.size());

        return all;
    }

    @Override
    public Participant save(Participant participant) {
        log.info("trying save participant [{}]", participant);
        Participant res = repo.save(participant);

        log.info("new participant saved [{}]", res);
        return res;
    }
}
