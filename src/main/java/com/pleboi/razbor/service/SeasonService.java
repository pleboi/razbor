package com.pleboi.razbor.service;

import com.pleboi.razbor.model.entity.Season;

import java.util.List;

public interface SeasonService {
    Season addNew();

    List<Integer> getSeasonNumbers();
}
