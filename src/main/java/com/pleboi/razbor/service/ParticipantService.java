package com.pleboi.razbor.service;

import com.pleboi.razbor.model.entity.Participant;

import java.io.InputStream;
import java.util.List;

public interface ParticipantService {
    List<Participant> saveAll(InputStream inputStream);

    List<Participant> findAll();

    Participant save(Participant participant);
}
