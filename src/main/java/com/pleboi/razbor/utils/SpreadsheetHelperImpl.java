package com.pleboi.razbor.utils;

import com.pleboi.razbor.model.dto.ParticipantDto;
import com.poiji.bind.Poiji;
import com.poiji.exception.PoijiExcelType;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.List;

@Component
public class SpreadsheetHelperImpl implements SpreadsheetHelper {
    @Override
    public List<ParticipantDto> excelToParticipant(InputStream is) {
        return Poiji.fromExcel(is, PoijiExcelType.XLS, ParticipantDto.class);
    }
}
