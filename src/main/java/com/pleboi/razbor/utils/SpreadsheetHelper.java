package com.pleboi.razbor.utils;

import com.pleboi.razbor.model.dto.ParticipantDto;

import java.io.InputStream;
import java.util.List;

public interface SpreadsheetHelper {
    List<ParticipantDto> excelToParticipant(InputStream is);
}
