package com.pleboi.razbor.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.pleboi.razbor.model.entity.Participant;
import com.poiji.annotation.ExcelCellName;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParticipantDto {
    @ExcelCellName("ID заявки")
    @NotNull(message = "field is required")
    private Long id;

    @ExcelCellName("ID пользователя")
    @NotNull(message = "field is required")
    private Long userVkId;

    @ExcelCellName("Дата отправки")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS", timezone = "GMT + 8")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dateTime;

    @ExcelCellName("Имя")
    @NotEmpty(message = "field is required")
    private String fullName;

    @ExcelCellName("Email адрес")
    private String email;

    @ExcelCellName("Ссылка на папку с файлами трек и текст (загруженная на диск)")
    @NotEmpty(message = "field is required")
    private String storageLink;

    @ExcelCellName("Ваша страница ВК")
    @NotEmpty(message = "field is required")
    private String vkLink;

    public Participant toParticipant() {
        Participant p = new Participant();
        p.setId(id);
        p.setUserVkId(userVkId);
        p.setFullName(fullName);
        p.setEmail(email);
        p.setStorageLink(storageLink);
        p.setVkLink(vkLink);
        p.setDateTime(dateTime);

        return p;
    }
}
