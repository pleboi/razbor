package com.pleboi.razbor.model.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Participant {
    @Id
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    private Long userVkId;

    private LocalDateTime dateTime;

    @Column(nullable = false)
    private String fullName;

    private String email;

    @Column(nullable = false)
    private String storageLink;

    @Column(nullable = false)
    private String vkLink;

    @Column(length = 500)
    private String comment;

    private String firstName;

    private String lastName;

    private String phone;

    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime createDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private LiveStream liveStream;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Participant that = (Participant) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
