package com.pleboi.razbor.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pleboi.razbor.api.service.ApiKeyService;
import com.pleboi.razbor.model.dto.ParticipantDto;
import com.pleboi.razbor.service.ParticipantService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.nio.charset.StandardCharsets;

import static com.pleboi.razbor.ParticipantTestUtils.createParticipantDto;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = {"razbor.value.apikey=123"})
@AutoConfigureMockMvc
@ActiveProfiles(value = "security")
class ParticipantRestControllerTest {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ApiKeyService apiKeyService;

    @Autowired
    private ParticipantService participantService;

    @Test
    void saveParticipant_expectResponse_isBadRequest() throws Exception {
        mockMvc.perform(post("/api/public/v1/participant")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .content(mapper.writeValueAsString(createParticipantDto(1)))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveParticipant_expectResponse_isUnauthorized() throws Exception {
        mockMvc.perform(post("/api/public/v1/participant")
                        .param("apiKey", "1234")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .content(mapper.writeValueAsString(createParticipantDto(1)))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void saveParticipant_expectResponse_isCreated() throws Exception {
        ParticipantDto participantDto = createParticipantDto(1);

        mockMvc.perform(post("/api/public/v1/participant")
                        .param("apiKey", "123")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .content(mapper.writeValueAsString(participantDto))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("id", Matchers.equalTo(participantDto.getId())))
                .andExpect(jsonPath("userVkId", Matchers.equalTo(participantDto.getUserVkId())))
                .andExpect(jsonPath("fullName", Matchers.equalTo(participantDto.getFullName())))
                .andExpect(jsonPath("vkLink", Matchers.equalTo(participantDto.getVkLink())))
                .andExpect(jsonPath("storageLink", Matchers.equalTo(participantDto.getStorageLink())))
                .andExpect(jsonPath("createDate", Matchers.notNullValue()));
    }

    @Test
    void saveParticipant_expectResponse_isBadRequest_violations() throws Exception {
        String json = mapper.writeValueAsString(ParticipantDto.builder().build());

        mockMvc.perform(post("/api/public/v1/participant")
                        .param("apiKey", "123")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .content(json)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("violations", Matchers.notNullValue()));
    }
}