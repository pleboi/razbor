package com.pleboi.razbor.service.impl;

import com.pleboi.razbor.ParticipantTestUtils;
import com.pleboi.razbor.model.dto.ParticipantDto;
import com.pleboi.razbor.model.entity.Participant;
import com.pleboi.razbor.repository.ParticipantRepository;
import com.pleboi.razbor.utils.SpreadsheetHelperImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ParticipantServiceImplTest {

    @Mock
    ParticipantRepository participantRepository;

    @Mock
    SpreadsheetHelperImpl spreadsheetHelper;

    @InjectMocks
    private ParticipantServiceImpl participantService;

    @Test
    void saveAll() throws FileNotFoundException {
        File initialFile = new File("src/test/resources/razbor-test-data.xls");
        InputStream is = new FileInputStream(initialFile);

        List<ParticipantDto> participantsDto = ParticipantTestUtils.createParticipantsDto(5);
        List<Participant> participants = participantsDto.stream().map(ParticipantDto::toParticipant).toList();

        when(spreadsheetHelper.excelToParticipant(any(InputStream.class)))
                .thenReturn(participantsDto);
        when(participantRepository.saveAll(anyList()))
                .thenReturn(participants);

        participantService.saveAll(is);
        verify(spreadsheetHelper, only()).excelToParticipant(any(InputStream.class));
        verify(participantRepository, only()).saveAll(anyList());
    }

}