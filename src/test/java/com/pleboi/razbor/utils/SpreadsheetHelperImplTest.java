package com.pleboi.razbor.utils;

import com.pleboi.razbor.model.dto.ParticipantDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SpreadsheetHelperImplTest {

    private static SpreadsheetHelper spreadsheetHelper;

    @BeforeAll
    static void init() {
        spreadsheetHelper = new SpreadsheetHelperImpl();
    }

    @Test
    void excelToParticipant() throws FileNotFoundException {
        File initialFile = new File("src/test/resources/razbor-test-data.xls");
        InputStream is = new FileInputStream(initialFile);

        List<ParticipantDto> participantDtoList = spreadsheetHelper.excelToParticipant(is);

        assertEquals(5, participantDtoList.size());
        participantDtoList.forEach(p -> {
            assertNotNull(p.getId());
            assertNotNull(p.getUserVkId());
            assertNotNull(p.getFullName());
            assertNotNull(p.getVkLink());
            assertNotNull(p.getStorageLink());
            assertNotNull(p.getDateTime());
            assertNotNull(p.getEmail());
        });
    }
}