package com.pleboi.razbor.repository;

import com.pleboi.razbor.model.entity.Participant;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static com.pleboi.razbor.ParticipantTestUtils.createParticipant;
import static com.pleboi.razbor.ParticipantTestUtils.createParticipants;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class ParticipantRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ParticipantRepository participantRepository;

    @Test
    public void whenFindById_thenReturnParticipant() {
        Participant participant = createParticipant(1);
        entityManager.persist(participant);
        entityManager.flush();

        Participant found = participantRepository.findById(participant.getId()).get();

        assertEquals(found.getId(), participant.getId());
    }

    @Test
    public void whenFindAll_thenReturnParticipants() {
        List<Participant> participants = createParticipants(3);
        participants.forEach(entityManager::persistAndFlush);

        List<Participant> found = participantRepository.findAll();

        assertEquals(found.size(), participants.size());
        assertTrue(participants.containsAll(found));
    }

    @Test
    public void whenSaveAll_thenAllPersist() {
        List<Participant> participants = createParticipants(4);

        participantRepository.saveAll(participants);

        participants.forEach(p -> {
            Participant participant = entityManager.find(Participant.class, p.getId());
            assertNotNull(participant);
        });
    }
}