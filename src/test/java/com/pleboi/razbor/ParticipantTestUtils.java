package com.pleboi.razbor;

import com.pleboi.razbor.model.dto.ParticipantDto;
import com.pleboi.razbor.model.entity.Participant;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ParticipantTestUtils {
    private final static String P_FULL_NAME = "Some Name";
    private final static String P_VK_LINK = "some_vk_link_";
    private final static String P_STORAGE_LINK = "some_storage_link_";

    public static ParticipantDto createParticipantDto(long id) {
        return ParticipantDto.builder()
                .id(id)
                .userVkId(id)
                .fullName(P_FULL_NAME + id)
                .vkLink(P_VK_LINK + id)
                .storageLink(P_STORAGE_LINK + id)
                .dateTime(LocalDateTime.now())
                .build();
    }

    public static Participant createParticipant(long id) {
        return createParticipantDto(id).toParticipant();
    }


    public static List<ParticipantDto> createParticipantsDto(int count) {
        List<ParticipantDto> participants = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            participants.add(createParticipantDto(i));
        }

        return participants;
    }

    public static List<Participant> createParticipants(int count) {
        List<Participant> participants = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            participants.add(createParticipant(i));
        }

        return participants;
    }
}
